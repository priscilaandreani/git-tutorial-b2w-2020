Tutorial prático para o curso de controle de versão para os estágiarios do primeiro semestre de 2020 da B2W.

Esse repo contém uma aplicação Flask que, dado uma lista de ids de produtos e os ids das suas ofertas, retorna o produto com a melhor avaliação.

Os estagiários devem se separar em times de 3 e realizar as seguintes tarefas:
 - Refatorar o código de cálculo de métricas
 - Dada as ofertas, usar a offer-api para recuperar o preço de cada uma delas e encontrar a oferta com o menor preço
 - Modularizar o código

As tarefas devem ser realizadas seguindo um fluxo de trabalho acordado por todos os integrantes do time.

O CI deve não pode estar com erro na branch master em momento algum.